from http.server import BaseHTTPRequestHandler
from urllib.parse import unquote
import ftfy

class handler(BaseHTTPRequestHandler):

    def do_GET(self):
        try:
            text = unquote(self.path[1:]) # Everything past /
            fixed = ftfy.fix_encoding(text)
            self.send_response(200)
            self.send_header('Content-Type', 'text/plain; charset=utf-8')
            self.end_headers()
            self.wfile.write(fixed.encode('utf-8'))
        except (TypeError, KeyError):
            self.send_error(400)
        except:
            self.send_error(500)
